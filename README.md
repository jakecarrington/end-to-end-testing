# About

![Cypress](https://www.cypress.io/img/logo-dark.36f3e062.png)

This project is mainly to test the Cypress front-end testing tool. For more info visit: https://www.cypress.io/

# Installing

Just simply clone the repo and run `npm i`

Cypress will take a while to download and install.

# Running

To run the interactive runner: `npx cypress open`

To run in terminal: `npx cypress run -b chrome` or `npm t`

# Adding tests

Test files should be placed in `/cypress/integration`

A couple of test files have been included already.

# Environment variables

Information such as usernames and passwords will be stored in env files. In Cypress' case, env variables are placed in the cypress.json file. This will need to be added manually to your local repo.
