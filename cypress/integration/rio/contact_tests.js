describe('Contact', async () => {
  beforeEach('login and go to contact page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 15000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('[name="headerName-NavLinkStyled-Contact"')
      .click();
  });

  it('page loads correctly', () => {
    cy.get('[name="main"]').contains('Contact information');
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
