describe('Reporting', async () => {
  beforeEach('login and go to reporting page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-Reporting')
      .click();
  });

  it('page loads correctly', () => {
    cy.get('[name^="reporting-menu"]', { timeout: 15000 });
  });

  it('click a report and check that it loads', () => {
    cy.get('[name="reporting-menu-Container"]', { timeout: 15000 })
      .find('div')
      .next()
      .first()
      .click()
      .get('[name="dundasBI"]')
      .find('iframe', { timeout: 15000 });
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
