describe('Documents', async () => {
  beforeEach('login and go to comply page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 15000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-Documents')
      .click();
  });

  it('page loads correctly', () => {
    cy.get('h2').contains('Documents');
  });

  it('upload document modal drop down data loads', () => {
    cy.get('[name="document-upload-button"]')
      .click()
      .get('[name="document-upload-modal-contractor__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-contractor__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('.rs__menu')
      .find('.rs__option', { timeout: 20000 })
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-ewc__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-ewc__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('.rs__menu')
      .find('.rs__option', { timeout: 20000 })
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-wastein__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-wastein__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('.rs__menu')
      .find('.rs__option', { timeout: 20000 })
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-wasteout__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-wasteout__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('.rs__menu')
      .find('.rs__option', { timeout: 20000 });
  });

  it('correct document types in list', () => {
    var docTypeOptionsExpected = [
      'Part A',
      'Part B',
      'Part C',
      'Part D',
      'Part E'
    ];
    var docTypeOptionsActual = [];
    cy.get('[name="document-upload-button"]')
      .click()
      .get('[name="document-upload-modal-documenttype__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('#document-upload-modal-uploadfile-IconStyled')
      .click()
      .get('[name="document-upload-modal-documenttype__select__container"]')
      .find('.rs__value-container')
      .click()
      .get('.rs__menu')
      .find('.rs__option', { timeout: 20000 })
      .each(o => docTypeOptionsActual.push(o.text()))
      .then(() =>
        assert(
          docTypeOptionsActual.toString() === docTypeOptionsExpected.toString()
        )
      );
  });

  it('checking Is Seasonal changes Collection Data selector to Start Date and End Date selectors', () => {
    cy.get('[name="document-upload-button"]')
      .click()
      .get('[name="document-upload-modal-collectionDate-input"]')
      .get('#document-upload-modal-seasonalContainer')
      .click()
      .get('[name="document-upload-modal-startDate-input"]')
      .get('[name="document-upload-modal-endDate-input"]');
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
