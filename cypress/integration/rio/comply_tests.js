describe('Comply/Legislation', async () => {
  beforeEach('login and go to comply page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 15000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-Comply')
      .click();
  });

  it('page loads correctly', function() {
    cy.get('[name="legislation-library__heading"]');
  });

  it('check that data loads', () => {
    cy.get('[name="legislation-library"]')
      .find('a')
      .find('a');
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
