describe('SYSADMIN', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click();
  });

  it('check if configuration menu item is there', () => {
    cy.get('#sidebarId-LinkTextStyled-Configuration');
  });
});

describe('BUSINESSADMIN', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('BUSINESSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('BUSINESSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click();
  });

  it('check if configuration menu item is there', () => {
    cy.get('#sidebarId-LinkTextStyled-Configuration');
  });
});

describe('BUSINESS', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('BUSINESS_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('BUSINESS_PASSWORD'))
      .get('[name="loginbutton"]')
      .click();
  });

  it('check if configuration menu item is there', () => {
    cy.get('#sidebarId-LinkTextStyled-Configuration').should('not.exist');
  });
});

describe('INDIVIDUAL', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('INDIVIDUAL_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('INDIVIDUAL_PASSWORD'))
      .get('[name="loginbutton"]')
      .click();
  });

  it('check if configuration menu item is there', () => {
    cy.get('#sidebarId-LinkTextStyled-Configuration').should('not.exist');
  });
});
