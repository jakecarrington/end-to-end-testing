describe('Data', async () => {
  beforeEach('login and go to data page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 15000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-Data')
      .click();
  });

  it.skip('page loads correctly', () => {
    cy.get('[name="configuration-menu-Container"]').contains('Data');
  });

  it.skip('check data loads', () => {
    cy.get('[name="in-progress__container"]').find('tbody');
  });

  it.skip('check uploads load', () => {
    cy.get('[name="data-menu__item--waste__upload"]')
      .find('div')
      .click()
      .get('[name="wasteDataUploadContainer"]')
      .find('a', { timeout: 10000 });
  });

  it.skip('open upload data modal', () => {
    cy.get('[name="data-menu__item--waste__upload"]')
      .find('div')
      .click()
      .get('[name="wasteDataUploadContainer__uploadButton"]')
      .click()
      .get('[name="wasteDataUploadContainer__uploadModal"]');
  });

  it('uploads data files', () => {
    cy.get('[name="data-menu__item--waste__upload"]')
      .children()
      .first()
      .click()
      .get('[name="wasteDataUploadContainer__uploadButton"]')
      .click()
      .get('[name="uploadProcess__pickFile__uploadFile"]')
      .type('C:\\Users\\JakeCarrington\\Desktop\\DittoTemplate.pdf');
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
