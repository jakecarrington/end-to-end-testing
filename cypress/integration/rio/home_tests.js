describe('Home', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 20000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click();
  });

  it('page loads correctly', () => {
    cy.get('[name="home-container"]').contains('Welcome to the Ditto');
  });

  it('click contact us', () => {
    cy.get('[name="home-container__button--contact"]')
      .click()
      .get('[name="main"]')
      .contains('Contact information');
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
