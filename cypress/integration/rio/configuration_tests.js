describe('Configuration', async () => {
  beforeEach('login and go to configuration page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="emailContainer"]', { timeout: 15000 })
      .find('input')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="passwordContainer"]')
      .find('input')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-Configuration')
      .click();
  });

  it('page loads correctly', () => {
    cy.get('[name="configuration-menu"]');
  });

  it('check users load', () => {
    cy.wait(2000)
      .get('[name="ConfigurationUsersContainer"]')
      .get('[name^="user-row"]', { timeout: 10000 });
  });

  it('check accounts load', () => {
    cy.get('[name="configuration-menu__item--accounts"]')
      .find('div')
      .contains('Accounts')
      .click()
      .get('[name="ConfigurationAccountsContainer"]')
      .get('[name^="account-row"]', { timeout: 10000 });
  });

  it('check contractors load', () => {
    cy.get('[name="configuration-menu__item--contractors"]')
      .find('div')
      .contains('Contractors')
      .click()
      .get('[name="ConfigurationContractorsContainer"]')
      .get('[name^="contractor-row"]', { timeout: 15000 });
  });

  it('check waste streams load', () => {
    cy.get('[name="configuration-menu__item--wastestream"]')
      .find('div')
      .contains('Waste Streams')
      .click()
      .get('[name$="WasteStream__Row"]', { timeout: 10000 });
  });

  it('check locations load', () => {
    cy.get('[name="configuration-menu__item--locations"]')
      .find('div')
      .contains('Locations')
      .click()
      .get('[name^="Location-row"]', { timeout: 10000 });
  });

  it('check tags load', () => {
    cy.get('[name="configuration-menu__item--tags"]')
      .find('div')
      .contains('Tags')
      .click()
      .get('[name^="tag-row"]', { timeout: 10000 });
  });

  afterEach('clear cookies', () => {
    cy.clearCookies();
  });
});
