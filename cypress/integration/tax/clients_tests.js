describe('My Clients', async () => {
  beforeEach('login and go to explore page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="email"')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="password"]')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkContainer-my-clients')
      .click();
  });

  it('does nothing', () => {});
});
