import faker from 'faker';
import parallel from 'mocha.parallel';

describe('Consultation', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="email"]')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="password"]')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkTextStyled-Explore')
      .click()
      .url()
      .should('equal', `${Cypress.env('URL')}/explore`)
      .get('p')
      .contains('Enterprise Investment Scheme')
      .click()
      .url()
      .should(
        'equal',
        `${Cypress.env('URL')}/explore/corporate/enterprise-investment-scheme`
      )
      .get('button')
      .contains('Start Guided Consultation')
      .click()
      .get('[placeholder="Untitled Application"]', { timeout: 10000 })
      .type(faker.commerce.productName())
      .get('.fa-check')
      .get('[name="undefined__check-button"]');
  });

  it('a', () => {});

  it('b', () => {});
});
