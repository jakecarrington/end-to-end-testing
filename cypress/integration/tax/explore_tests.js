describe('Explore', async () => {
  beforeEach('login and go to home page', () => {
    cy.visit(Cypress.env('URL'))
      .get('[name="email"')
      .type(Cypress.env('SYSADMIN_USERNAME'))
      .get('[name="password"]')
      .type(Cypress.env('SYSADMIN_PASSWORD'))
      .get('[name="loginbutton"]')
      .click()
      .get('#sidebarId-LinkTextStyled-Explore')
      .click()
      .url()
      .should('equal', `${Cypress.env('URL')}/explore`);
  });

  it('click on corporate taxes link', () => {
    cy.get('a > div > :nth-child(1) > #undefined_image')
      .click()
      .url()
      .should('equal', `${Cypress.env('URL')}/explore/corporate`);
  });

  it('click on enterprise investment scheme link', () => {
    cy.get('p')
      .contains('Enterprise Investment Scheme')
      .click()
      .url()
      .should(
        'equal',
        `${Cypress.env('URL')}/explore/corporate/enterprise-investment-scheme`
      );
  });
});
