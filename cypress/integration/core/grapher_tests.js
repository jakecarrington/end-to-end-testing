import * as faker from 'faker';

describe('Builder Index', async () => {
  let kbName;

  beforeEach('create knowledgebase and open it', () => {
    kbName = `${faker.commerce.color()} ${faker.commerce
      .productName()
      .toLowerCase()}`;

    cy.visit('http://qa-adminportal.ditto.ai/')
      .get('#Email')
      .type('jake.carrington@ditto.ai')
      .get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('#builderMenuItem')
      .click()
      .get('#kbAdd')
      .click()
      .get('#newKbName')
      .type(kbName)
      .get('#saveKbBtn')
      .click()
      .wait(1000)
      .get('a')
      .contains(kbName)
      .click();
  });

  it.skip('create root node', () => {
    cy.get('#addItemExplorer')
      .click()
      .get('#newTreeItemName')
      .type('root')
      .get('#methodTypeFieldSet > [value="Graph"]')
      .click()
      .get('#saveFactor')
      .click()
      .reload()
      .wait(3000)
      .window()
      .then(win =>
        win.setUpRobots().then(() => {
          assert(
            win.robotGrapher.diagram.nodes.first().data.FactorNickname ===
              'root'
          );
        })
      );
  });

  it('create root node and add factor', () => {
    cy.get('#addItemExplorer')
      .click()
      .get('#newTreeItemName')
      .type('root')
      .get('#methodTypeFieldSet > [value="Graph"]')
      .click()
      .get('#saveFactor')
      .click()
      .reload()
      .wait(500)
      .window()
      .then(win =>
        win.setUpRobots().then(() => {
          var nodes = win.robotGrapher.diagram.nodes;
          assert(nodes.first().data.FactorNickname === 'root');
          cy.get('#addItemExplorer')
            .click()
            .get('#newTreeItemName')
            .type('alpha')
            .get('#methodTypeFieldSet > [value="Graph"]')
            .click()
            .get('#saveFactor')
            .click();
        })
      );
  });

  afterEach('go to index page and delete knowledgebase', () => {
    cy.get('#builderMenuItem')
      .click()
      .get('a')
      .contains(kbName)
      .parentsUntil('#contenttablejqxgrid')
      .eq(2)
      .find('.jqx-checkbox-default')
      .find('span')
      .parent()
      .click()
      .get('#kbDelete')
      .click()
      .wait(500)
      .get('.ui-dialog')
      .find('.ui-button')
      .contains('Ok')
      .click()
      .get('a')
      .contains(kbName)
      .should('not.exist');
  });
});
