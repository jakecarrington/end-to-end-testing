describe('Login', async () => {
  beforeEach('go to login page', () => {
    cy.visit('http://qa-adminportal.ditto.ai/');
    //   .get('#Email')
    //   .type('jake.carrington@ditto.ai')
    //   .get('#Password')
    //   .type('Emp1r1c0m')
    //   .get('#loginBtn')
    //   .click();
  });

  it('valid login', () => {
    cy.get('#Email')
      .type('jake.carrington@ditto.ai')
      .get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('#builderMenuItem');
  });

  it('invalid email', () => {
    cy.get('#Email')
      .type('jake.carrington')
      .get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('.field-validation-error')
      .contains('The Email field is not a valid e-mail address.');
  });

  it('wrong email', () => {
    cy.get('#Email')
      .type('jake@ditto.ai')
      .get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('.validation-summary-errors')
      .contains('Invalid login attempt');
  });

  it('wrong password', () => {
    cy.get('#Email')
      .type('jake.carrington@ditto.ai')
      .get('#Password')
      .type('wrong password')
      .get('#loginBtn')
      .click()
      .get('.validation-summary-errors')
      .contains('Invalid login attempt');
  });

  it('no credentials', () => {
    cy.get('#loginBtn')
      .click()
      .get('.field-validation-error')
      .contains('The Email field is required.')
      .get('.field-validation-error')
      .contains('The Password field is required.');
  });

  it('no email', () => {
    cy.get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('.field-validation-error')
      .contains('The Email field is required.');
  });

  it('no password', () => {
    cy.get('#Email')
      .type('jake.carrington@ditto.ai')
      .get('#loginBtn')
      .click()
      .get('.field-validation-error')
      .contains('The Password field is required.');
  });
});
