import * as faker from 'faker';

describe('Builder Index', async () => {
  beforeEach('login and go to builder index page', () => {
    cy.visit('http://qa-adminportal.ditto.ai/')
      .get('#Email')
      .type('jake.carrington@ditto.ai')
      .get('#Password')
      .type('Emp1r1c0m')
      .get('#loginBtn')
      .click()
      .get('#builderMenuItem')
      .click();
  });

  it('Add and delete knowledgebase', () => {
    var kbName = `${faker.commerce.color()} ${faker.commerce
      .productName()
      .toLowerCase()}`;

    cy.get('#kbAdd')
      .click()
      .get('#newKbName')
      .type(kbName)
      .get('#saveKbBtn')
      .click()
      .wait(1000)
      .get('a')
      .contains(kbName)
      .parentsUntil('#contenttablejqxgrid')
      .eq(2)
      .find('.jqx-checkbox-default')
      .find('span')
      .parent()
      .click()
      .get('#kbDelete')
      .click()
      .wait(500)
      .get('.ui-dialog')
      .find('.ui-button')
      .contains('Ok')
      .click()
      .get('a')
      .contains(kbName)
      .should('not.exist');
  });
});
