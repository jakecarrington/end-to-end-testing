import groovy.io.FileType

node {
  def proj = 'rio'
  def env = 'qa'

  def result = 'SUCCESS'
  catchError {
    stage('Preparation') {
        //clear workspace
        dir ('mochawesome-reports') {
          deleteDir()
        }

        dir("${JENKINS_HOME}\\jobs\\${JOB_NAME}\\htmlreports") {
          deleteDir()
        }

        // Get some code from a GitHub repository
        git branch: "master", credentialsId: '4980a999-337a-4548-bb0d-cc8b551a77ac', url: "https://bitbucket.org/jakecarrington/end-to-end-testing.git"

        //install tools
        bat "npm install"
    }
    stage('Test') {
      try {
        bat "set PROJ=${proj}&& set ENV=${env}&& npm t"
      }
      catch (e) {
        result = 'UNSTABLE'
      }
    }
    stage('Publish HTML Reports') {
      File folder = new File("C:/Builds/${JOB_NAME}/cypress/integration/${proj}")
      File[] listOfFiles = folder.listFiles();
      String csList = ""
      for (int i = 0; i < listOfFiles.length; i++) {
        csList += listOfFiles[i].getName()
      }
      println csList
      publishHTML([
        allowMissing: false,
        alwaysLinkToLastBuild: true,
        keepAll: true, 
        reportDir: '\\mochawesome-reports', 
        reportFiles: 'mochawesome.html, mochawesome_001.html, mochawesome_002.html, mochawesome_003.html, mochawesome_004.html, mochawesome_005.html, mochawesome_006.html', 
        reportName: "Test Results #${BUILD_NUMBER}"
      ])
      currentBuild.result = result
    }
  }
}