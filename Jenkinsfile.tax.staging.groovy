node {
  def proj = 'tax'
  def env = 'staging'

  def result = 'SUCCESS'
  catchError {
    stage('Preparation') {
        //clear workspace
        dir ('mochawesome-reports') {
          deleteDir()
        }

        dir("${JENKINS_HOME}\\jobs\\${JOB_NAME}\\htmlreports") {
          deleteDir()
        }

        // Get some code from a GitHub repository
        git branch: "master", credentialsId: '4980a999-337a-4548-bb0d-cc8b551a77ac', url: "https://bitbucket.org/jakecarrington/end-to-end-testing.git"

        //install tools
        bat "npm install"
    }
    stage('Test') {
      try {
        bat "set PROJ=${proj}&& set ENV=${env}&& npm t"
      }
      catch (e) {
        result = 'UNSTABLE'
      }
    }
    stage('Publish HTML Reports') {
      publishHTML([
        allowMissing: false,
        alwaysLinkToLastBuild: true,
        keepAll: true, 
        reportDir: '\\mochawesome-reports', 
        reportFiles: 'mochawesome.html, mochawesome_001.html, mochawesome_002.html, mochawesome_003.html, mochawesome_004.html, mochawesome_005.html', 
        reportName: "Test Results #${BUILD_NUMBER}"
      ])
      currentBuild.result = result
    }
  }
}