const cypress = require('cypress');
const opts = require('./cypress.json');
const rimraf = require('rimraf');
const path = require('path');
const uuidv4 = require('uuid/v4');
const marge = require('mochawesome-report-generator');
const fs = require('fs');

const combine = require('./reports/utils/combine');
(() => {
  cypress.run(opts);
})();
